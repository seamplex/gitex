# GiTeX

A high-yet-still-pretty-low-level layer for document preparation and management: Git + (Xe)LaTeX + (+ Bash \& M4 + optionally Markdown)

## Dependencies

Make sure you have the following tools installed.

### Required

 * Git
 * XeLaTeX
 * Bash
 * M4

```
sudo apt-get install bash m4 sed git texlive-xetex texlive-latex-recommended texlive-fonts-recommended texlive-science
```

### Optional

 * Biber
 * Inkscape
 * Latexdiff

## Usage

 1. Clone the repository into a new directory that will contain your report.
 2. Edit `fields.md` and fill in the report meta-data.
 3. Edit `body.tex` and fill in the report body.
 4. Run `./updatebase.sh` to fix the initial revision
 5. Commit your changes 
      $ git add .
      $ git commit
 6. Run `./gitex.sh` to compile the report. You can find the actual version in `current.pdf`.
 7. Continue editing, commiting and compiling until you get a finished issuable version.
 8. Tag the last commit and mark it as revision A:
      $ git -a A -m "First issue"
 9. Compile again with `./gitex.sh`. Now your report will be marked as revision A and “First issue” will be the description in the revision sheet.
 10. Continue editing, commiting, compiling and tagging for newer revisions.

