#!/bin/bash 
#
# usage:
#  $ texdiff.sh old new
# where old & new are references to revisions (i.e. A, C2 or DRAFT4+)
# that should already exist as tex files
#
# the output is a document called diff-{old}-{new}.pdf
#
# TODO: write a hgdiff.sh that accepts mercurial revisions to get a diff
#

if [ -z `which latexdiff` ]; then
  echo "error: latexdiff not installed"
  exit 1
fi

if [[ -z "$1" ]] || [[ -z "$2" ]]; then
  echo usage: $0 old new
  exit 1
fi

old=$1
new=$2

docnumber=`echo gitex_docnumber | m4 fields.m4 -`

if [ ! -e ${docnumber}-${old}.tex ]; then
  echo "${docnumber}-${old}.tex does not exist :-("
  exit 1
fi

if [ ! -e ${docnumber}-${new}.tex ]; then
  echo "${docnumber}-${new}.tex does not exist :-("
  exit 1
fi

# package hyperref does not get along with latexdiff
sed s/\\\\usepackage{hyperref/"% \\\\usepackage{hyperref"/ ${docnumber}-${old}.tex | \
   sed s/\\\\hyper/"% \\\\hyper"/ | sed s/\\\\href{.*}{/{/ | sed s/\\\\afterpage// | \
   sed s/\\\\toprule/"% \\\\toprule"/ | \
   sed s/\\\\midrule/"% \\\\midrule"/ | \
   sed s/\\\\bottomrul/"% \\\\bottomrule"/ > old.tex
sed s/\\\\usepackage{hyperref/"% \\\\usepackage{hyperref"/ ${docnumber}-${new}.tex | \
   sed s/\\\\hyper/"% \\\\hyper"/ | sed s/\\\\href{.*}{/{/ | sed s/\\\\afterpage// | \
   sed s/\\\\toprule/"% \\\\toprule"/ | \
   sed s/\\\\midrule/"% \\\\midrule"/ | \
   sed s/\\\\bottomrule/"% \\\\bottomrule"/ > new.tex

latexdiff --encoding=utf8 old.tex new.tex > diff-${old}-${new}.tex


# choose a latex backend
if [ ! -z "`which xelatex`" ]; then
  latexbin=xelatex
elif [ ! -z "`which pdflatex`" ]; then
  latexbin=pdflatex
elif [ ! -z "`which latex`" ]; then
  latexbin=latex
else
  echo "error: it seems that LaTeX is not installed"
fi

# choose a bibtex backend
if [ ! -z "`which biber`" ]; then
  bibtexbin=biber
elif [ ! -z "`which bibtex`" ]; then
  bibtexbin=bibtex
fi


# TODO: repeat until no undefined references found (or max iterations)
# TODO: check if the bibliography changed
${latexbin} -interaction=nonstopmode diff-${old}-${new}.tex
if [ ! -z "${bibtexbin}" ]; then
  ${bibtexbin} diff-${old}-${new}
fi
${latexbin} -interaction=nonstopmode diff-${old}-${new}.tex
${latexbin} -interaction=nonstopmode diff-${old}-${new}.tex

# rm old.tex new.tex
